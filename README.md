# #TheNextBigFin - Android Challenge

##It was great and fun to develop this Challenge

##Follow some informations

* this application has two Flavors, actual(which hits the actual API) and mock (which doesn't hits the actual API), please have sure that you are in the right flavor during testing the app
* the archutecture is MVVM using dagger2, RX and LiveData (only for view and viewmodel comunication)
* the application has three modules datacontracts, datamodule and datamockmodule
* the datacontracts has de dataclasses and the contracts for data providers
* the datamodule has the implementation of the data providers which actually hits de API
* the datamockmodule has the implementation of the data providers which doesnt hits de API, it is for developing the view without having to wait for API calls

##I Hope you guys enjoy this challenge

##Thanks


![example_app.png](https://bitbucket.org/TheNextBigFin/androidchallenge/raw/master/example_app.png)


#### Sua sprint começou e você precisa desenvolver um aplicativo que possibilite as pessoas acompanharem a cotação de Criptomoedas em tempo real utilizando as api's do CoinCap. 

### O Aplicativo:

#### Crie uma lista com todas as Criptomoedas disponíveis no CoinCap. Cada item dessa lista deve conter:
* Nome
* Símbolo
* Cotação atual
* Variação atual
* Thumbnail

#### Ao clicar em um item da lista o aplicativo deve apresentar uma tela de detalhe contendo as seguintes informações:
* Market Cap
* Available Supply
* Volume de transações do dia 

### Requisitos:
* O usuário poderá buscar pelo nome da moeda. Desenvolva um sistema de busca;
* Desenvolva em Java ou Kotlin;
* Arquitetura limpa com clara divisão de responsabilidades;
* Testes unitários e instrumentados;

### Ganha mais pontos:
* Utilize um framework reativo;
* Utilize algum Http Client para consultar as Api's
* Cache das Imagens
* Endless scroll na listagem

### Algumas informações:

* Api's CoinCap
https://github.com/CoinCapDev/CoinCap.io


* Url dos thumbnails
https://coincap.io/images/coins/bitcoin.png (Altere o final para o nome completo)

### Submissão:
Faça um fork do projeto e ao final um Pull Request para análise. Não esqueça de deixar o Pull Request público para análise do desafio.

### Prazo de Entrega
4 Dias

### Boa Sorte ;)