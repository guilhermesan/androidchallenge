package com.guilhermesan.datacontracts.dataproviders

import android.content.Context
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import io.reactivex.Observable


abstract class CoinDataProvider(context: Context) {

    abstract fun getCoins():Observable<List<Coin>>
    abstract fun getCoins(query:String):Observable<List<Coin>>
    abstract fun getCoinDetail(symbol:String):Observable<CoinDetail>
    abstract fun evictCache()

}