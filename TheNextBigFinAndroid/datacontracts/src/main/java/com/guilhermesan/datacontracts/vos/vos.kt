package com.guilhermesan.datacontracts.vos

import java.io.Serializable

data class Coin(val name: String, val symbol: String, val price: Double, val variation: Double, val thumbUrl: String) : Serializable

data class CoinDetail(val marketCap: Double, val todayVolume: Double, val availableSupply: Double, val coin: Coin) : Serializable
