package com.guilhermesan.datacontracts.core

import android.content.Context
import android.util.Log
import com.guilhermesan.datacontracts.vos.Coin
import io.reactivex.Observable
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream


class CacheManager(val context: Context) {

    val KEY_COIN_LIST = "coin_list.cache"

    @Throws(IOException::class)
    private fun writeObject(key: String, obj: Any) {
        val fos = context.openFileOutput(key, Context.MODE_PRIVATE)
        val oos = ObjectOutputStream(fos)
        oos.writeObject(obj)
        oos.close()
        fos.close()
    }

    @Throws(IOException::class, ClassNotFoundException::class)
    private fun readObject(key: String): Any {
        val fis = context.openFileInput(key)
        val ois = ObjectInputStream(fis)
        return ois.readObject()
    }

    @Throws(IOException::class, ClassNotFoundException::class)
    private fun destroyObject(key: String): Any {
        return context.deleteFile(key)
    }

    fun cacheCoins(list: List<Coin>){
        try {
            writeObject(KEY_COIN_LIST,list)
        }catch (e:Exception){
            Log.e("error",e.message,e)
        }
    }

    fun getCoins():Observable<List<Coin>>{
        try {
            var coins = readObject(KEY_COIN_LIST) as List<Coin>
            return Observable.just(coins)
        }catch (e:Exception){
            Log.e("error",e.message,e)
        }
        return Observable.just(emptyList())

    }

    fun evictCoins(){
        destroyObject(KEY_COIN_LIST)
    }

};