package com.guilhermesan.apimodule.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import java.lang.reflect.Type

class CoinDetailDeserializer : JsonDeserializer<CoinDetail> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): CoinDetail {
        val coinJson = json.asJsonObject
        val name = coinJson.get("display_name").asString
        val symbol = coinJson.get("id").asString
        val price = coinJson.get("price").asDouble
        val variation = coinJson.get("cap24hrChange").asDouble
        val volumeToday = coinJson.get("volume").asDouble
        val availableSupply = coinJson.get("supply").asDouble
        val marcketCap = coinJson.get("market_cap").asDouble
        val thumb = "http://coincap.io/images/coins/$name.png"
        val coin = Coin(name, symbol, price, variation, thumb)
        return CoinDetail(marcketCap,volumeToday,availableSupply,coin)
    }

}