package com.guilhermesan.apimodule.providers

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.guilhermesan.apimodule.deserializers.CoinDeserializer
import com.guilhermesan.apimodule.deserializers.CoinDetailDeserializer
import com.guilhermesan.datacontracts.dataproviders.CoinDataProvider
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import io.reactivex.Observable


class CoinDataProviderImp(context: Context) : CoinDataProvider(context) {

    override fun evictCache() {
        //ignore
    }

    override fun getCoins(query: String): Observable<List<Coin>> {
        return getCoins().map {
            it.filter { it.name.toLowerCase().contains(query.toLowerCase()) }
        }
    }

    fun provideFakeCoinList():List<Coin>{
        val json = FakeJsons.coinsJson()
        val listType = object : TypeToken<List<Coin>>() {}.type
        return GsonBuilder()
                .registerTypeAdapter(Coin::class.java,CoinDeserializer())
                .create()
                .fromJson(json, listType)
    }

    fun provideFakeCoinDetail():CoinDetail{
        val json = FakeJsons.detailJson()
        return GsonBuilder()
                .registerTypeAdapter(CoinDetail::class.java,CoinDetailDeserializer())
                .create()
                .fromJson(json, CoinDetail::class.java)
    }

    override fun getCoins(): Observable<List<Coin>> {
        return Observable.just(provideFakeCoinList())
    }

    override fun getCoinDetail(symbol: String): Observable<CoinDetail> {
        return Observable.just(provideFakeCoinDetail())
    }


}