package com.guilhermesan.apimodule.providers

class FakeJsons {
    companion object {



        fun coinsJson():String{
            return "[{\"cap24hrChange\":-3.3,\"long\":\"Bitcoin\",\"mktcap\":153233183362.5,\"perc\":-3.3,\"price\":9008.55,\"shapeshift\":true,\"short\":\"BTC\",\"supply\":17009750,\"usdVolume\":7967810000,\"volume\":7967810000,\"vwapData\":9046.441410630896,\"vwapDataBTC\":9046.441410630896},{\"cap24hrChange\":-2.37,\"long\":\"Ethereum\",\"mktcap\":65728670207.04,\"perc\":-2.37,\"price\":662.798,\"shapeshift\":true,\"short\":\"ETH\",\"supply\":99168480,\"usdVolume\":2741200000,\"volume\":2741200000,\"vwapData\":654.9105103548908,\"vwapDataBTC\":654.9105103548908},{\"cap24hrChange\":-1.6,\"long\":\"Ripple\",\"mktcap\":32709166315.17324,\"perc\":-1.6,\"price\":0.835401,\"shapeshift\":true,\"short\":\"XRP\",\"supply\":39153851043,\"usdVolume\":638636000,\"volume\":638636000,\"vwapData\":0.8135891409897493,\"vwapDataBTC\":0.8135891409897493},{\"cap24hrChange\":-6.5,\"long\":\"Bitcoin Cash\",\"mktcap\":22156658862.820004,\"perc\":-6.5,\"price\":1295.39,\"shapeshift\":true,\"short\":\"BCH\",\"supply\":17104238,\"usdVolume\":783122000,\"volume\":783122000,\"vwapData\":1301.963617893106,\"vwapDataBTC\":1301.963617893106},{\"cap24hrChange\":-3.16,\"long\":\"EOS\",\"mktcap\":14477194872.9803,\"perc\":-3.16,\"price\":17.4659,\"shapeshift\":true,\"short\":\"EOS\",\"supply\":828883417,\"usdVolume\":3490360000,\"volume\":3490360000,\"vwapData\":17.037626671282286,\"vwapDataBTC\":17.037626671282286},{\"cap24hrChange\":2.63,\"long\":\"Cardano\",\"mktcap\":9259931023.858315,\"perc\":2.63,\"price\":0.357153,\"shapeshift\":false,\"short\":\"ADA\",\"supply\":25927070538,\"usdVolume\":371657000,\"volume\":371657000,\"vwapData\":0.3365376071935019,\"vwapDataBTC\":0.3365376071935019}]"
        }

        fun detailJson():String{
            return "{\"altCap\":268642267232.62097,\"bitnodesCount\":10465,\"btcCap\":154939658705.59998,\"btcPrice\":9081.225435715112,\"dom\":51.21,\"totalCap\":423581925938.2223,\"volumeAlt\":1005135598.4365594,\"volumeBtc\":1055133024.018824,\"volumeTotal\":2060268622.4553838,\"id\":\"BTC\",\"type\":\"cmc\",\"_id\":\"179bd7dc-72b3-4eee-b373-e719a9489ed9\",\"price_btc\":1,\"price_eth\":13.484671949710215,\"price_ltc\":61.698477813504084,\"price_zec\":31.613454945474583,\"price_eur\":7561.880240308739,\"price_usd\":9081.225435715112,\"market_cap\":154939658705.59998,\"cap24hrChange\":-1.61,\"display_name\":\"Bitcoin\",\"status\":\"available\",\"supply\":17009887,\"volume\":7822300000,\"price\":9108.8,\"vwap_h24\":9016.236769726347,\"rank\":1,\"alt_name\":\"bitcoin\"}"
        }
    }
}