package com.guilhermesan.thenextbigfin.ui.screens.currencylist

import android.app.Application
import android.app.SearchManager
import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import com.guilhermesan.datacontracts.dataproviders.CoinDataProvider
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.thenextbigfin.extencions.addToDisposeBag
import com.guilhermesan.thenextbigfin.ui.screens.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject



class CurrencyListViewModel(application: Application): BaseViewModel(application) {

    @Inject
    lateinit var coinDataProvider: CoinDataProvider

    private val querySubject = PublishSubject.create<String>()

    init {
        getComponent().inject(this)
        querySubject
                .throttleLast(200,TimeUnit.MILLISECONDS)
                .subscribe { query ->
                    coinDataProvider.getCoins(query)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ list ->
                                coinsLiveData.value = list
                                hiddeLoading()
                            },{
                                hiddeLoading()
                            }).addToDisposeBag(disposeBag)
                }.addToDisposeBag(disposeBag)
    }

    val coinsLiveData = MutableLiveData<List<Coin>>()



    fun handleIntent(intent: Intent){
        if (Intent.ACTION_SEARCH == intent.action) {
            search(intent.getStringExtra(SearchManager.QUERY))
        } else {
            search("")
        }
    }

    fun onCreate(){
        coinDataProvider.evictCache()
    }


    fun search(query:String){
        querySubject.onNext(query)
    }

}