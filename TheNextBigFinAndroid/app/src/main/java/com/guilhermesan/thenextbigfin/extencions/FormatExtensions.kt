package com.guilhermesan.thenextbigfin.extencions

import java.text.NumberFormat
import java.util.*

fun Double.formatCurrency():String{
    val numberFormat = NumberFormat.getCurrencyInstance(Locale.US)
    return numberFormat.format(this)
}

fun Double.formatVariation():String{
    val numberFormat = NumberFormat.getPercentInstance()
    numberFormat.maximumFractionDigits = 2
    return (if (this > 0)  "+ "  else "- ") + numberFormat.format(Math.abs(this)/100)
}