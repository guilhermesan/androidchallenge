package com.guilhermesan.thenextbigfin.ui.screens.currencylist

import android.app.Activity
import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import com.guilhermesan.thenextbigfin.R
import com.guilhermesan.thenextbigfin.ui.screens.BaseActivity
import com.guilhermesan.thenextbigfin.ui.screens.currencydetail.CurrencyDetailActivity
import kotlinx.android.synthetic.main.activity_currency_list.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject


class CurrencyListActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: CurrencyListViewModel

    @Inject
    lateinit var currencyAdapter: CurrencyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_list)
        onCreated()
        viewModel.onCreate()
        viewModel.handleIntent(intent)
    }

    override fun subscribe() {
        with(viewModel) {
            coinsLiveData.observe(this@CurrencyListActivity, Observer { list ->
                if (list != null) currencyAdapter.update(list)
            })

            isLoading.observe(this@CurrencyListActivity, Observer { isLoading ->
                if (isLoading != null && isLoading) {
                    rvList.visibility = View.GONE
                    loadingView.visibility = View.VISIBLE
                } else {
                    rvList.visibility = View.VISIBLE
                    loadingView.visibility = View.GONE
                }
            })

        }
        currencyAdapter.coinSelectedLiveData.observe(this, Observer {coin ->
            if (coin != null) {
                CurrencyDetailActivity.start(this, coin)
            }
        })
    }

    override fun setup(){
        setupToolBar(toolbar,getString(R.string.activity_list_title),false)
        rvList.layoutManager = LinearLayoutManager(this)
        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        rvList.addItemDecoration(decoration)
        rvList.adapter = currencyAdapter

    }

    companion object {
        fun start(activity: Activity){
            activity.startActivity<CurrencyListActivity>()
            activity.overridePendingTransition(R.anim.apear,R.anim.desapear)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (menu != null) {
            val inflater = menuInflater
            inflater.inflate(R.menu.list_menu, menu)

            // Associate searchable configuration with the SearchView
            val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
            val searchView = menu.findItem(R.id.search).actionView as SearchView
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

            searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    if (query != null) {
                        val searchIntent = Intent(this@CurrencyListActivity, CurrencyListActivity::class.java)
                        searchIntent.action = Intent.ACTION_SEARCH
                        searchIntent.putExtra(SearchManager.QUERY, query)
                        startActivity(searchIntent)
                        return true
                    }
                    return false
                }

            })

            return true
        }
        return false
    }

    override fun onNewIntent(intent: Intent) {
        viewModel.handleIntent(intent)
    }


}
