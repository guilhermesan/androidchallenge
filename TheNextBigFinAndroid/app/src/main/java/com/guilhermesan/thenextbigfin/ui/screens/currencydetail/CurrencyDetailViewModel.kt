package com.guilhermesan.thenextbigfin.ui.screens.currencydetail

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.guilhermesan.datacontracts.dataproviders.CoinDataProvider
import com.guilhermesan.datacontracts.vos.CoinDetail
import com.guilhermesan.thenextbigfin.extencions.addToDisposeBag
import com.guilhermesan.thenextbigfin.ui.screens.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CurrencyDetailViewModel(application: Application) : BaseViewModel(application) {

    @Inject
    lateinit var coinDataProvider: CoinDataProvider

    val coinLiveData = MutableLiveData<CoinDetail>()

    init {
        getComponent().inject(this)
    }

    fun onCreate(symbol: String) {
        showLoading()
        coinDataProvider
                .getCoinDetail(symbol)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ coin ->
                    hiddeLoading()
                    coinLiveData.value = coin
                }, {
                    if (it != null) {
                        Log.e("error", it.message)
                    }
                    hiddeLoading()
                }).addToDisposeBag(disposeBag)
    }


}