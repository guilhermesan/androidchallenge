package com.guilhermesan.thenextbigfin.dagger

import com.guilhermesan.thenextbigfin.TheNextBigFinApp
import com.guilhermesan.thenextbigfin.ui.screens.BaseViewModel
import com.guilhermesan.thenextbigfin.ui.screens.currencydetail.CurrencyDetailViewModel
import com.guilhermesan.thenextbigfin.ui.screens.currencylist.CurrencyListViewModel
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@Component(modules = [AppModule::class, AndroidSupportInjectionModule::class, BuildersModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: TheNextBigFinApp): Builder
        fun build(): AppComponent

    }

    fun inject(app: TheNextBigFinApp)
    fun inject(currencyListViewModel: CurrencyListViewModel)
    fun inject(currencyDetailViewModel: CurrencyDetailViewModel)
    fun inject(baseViewModel: BaseViewModel)
}


