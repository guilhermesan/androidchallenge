package com.guilhermesan.thenextbigfin.ui.screens

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.guilhermesan.thenextbigfin.TheNextBigFinApp
import com.guilhermesan.thenextbigfin.dagger.AppComponent
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val disposeBag = CompositeDisposable()

    var isLoading: MutableLiveData<Boolean> = MutableLiveData()



    fun showLoading() {
        isLoading.value = true
    }

    fun hiddeLoading() {
        isLoading.value = false
    }

    fun getComponent():AppComponent {
        return getApplication<TheNextBigFinApp>().appComponent
    }

    override fun onCleared() {
        disposeBag.dispose()
        super.onCleared()
    }

}