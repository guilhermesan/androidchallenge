package com.guilhermesan.thenextbigfin.dagger

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import com.guilhermesan.apimodule.providers.CoinDataProviderImp
import com.guilhermesan.datacontracts.dataproviders.CoinDataProvider
import com.guilhermesan.thenextbigfin.TheNextBigFinApp
import com.guilhermesan.thenextbigfin.ui.screens.currencydetail.CurrencyDetailActivity
import com.guilhermesan.thenextbigfin.ui.screens.currencydetail.CurrencyDetailViewModel
import com.guilhermesan.thenextbigfin.ui.screens.currencylist.CurrencyAdapter
import com.guilhermesan.thenextbigfin.ui.screens.currencylist.CurrencyListActivity
import com.guilhermesan.thenextbigfin.ui.screens.currencylist.CurrencyListViewModel
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun currencyListActivity():CurrencyListActivity

    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun currencyDetailActivity():CurrencyDetailActivity

}


@Module()
open class AppModule {

    @Provides
    fun provideContext(application: TheNextBigFinApp): Context {
        return application.applicationContext
    }

    @Provides
    fun providesCurrencyListViewModel(currencyListActivity: CurrencyListActivity): CurrencyListViewModel {
        return ViewModelProviders.of(currencyListActivity).get(CurrencyListViewModel::class.java)
    }

    @Provides
    fun providesCurrencyDetailViewModel(currencyDetailActivity: CurrencyDetailActivity): CurrencyDetailViewModel {
        return ViewModelProviders.of(currencyDetailActivity).get(CurrencyDetailViewModel::class.java)
    }

    @Provides
    fun providesCurrencyAdapter(currencyListActivity: CurrencyListActivity): CurrencyAdapter {
        return CurrencyAdapter(listOf(),currencyListActivity)
    }

    @Provides
    fun providesCoinDataProvider(application: TheNextBigFinApp): CoinDataProvider {
        return CoinDataProviderImp(application)
    }


}
