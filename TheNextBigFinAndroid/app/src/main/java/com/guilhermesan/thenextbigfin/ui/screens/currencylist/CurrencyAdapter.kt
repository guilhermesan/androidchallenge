package com.guilhermesan.thenextbigfin.ui.screens.currencylist

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.thenextbigfin.R
import com.guilhermesan.thenextbigfin.extencions.formatCurrency
import com.guilhermesan.thenextbigfin.extencions.formatVariation
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_coin_view.view.*
import org.jetbrains.anko.textColor
import java.lang.Exception

class CurrencyAdapter constructor(var list: List<Coin>, var context: Context) : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    var inflater: LayoutInflater = LayoutInflater.from(context)

    val coinSelectedLiveData = MutableLiveData<Coin>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = inflater.inflate(R.layout.list_coin_view, parent, false)
        return CurrencyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class CurrencyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bind(coin: Coin) {
            view.tvName.text = coin.name
            view.tvPrice.text = coin.price.formatCurrency()
            view.tvSymbol.text = coin.symbol
            view.tvVariation.text = coin.variation.formatVariation()
            view.tvVariation.textColor = ContextCompat.getColor(
                    context,
                    if (coin.variation > 0)
                        R.color.green_positive
                    else
                        R.color.red_negative)

            Picasso.get()
                    .load(coin.thumbUrl)
                    .into(view.ivCoinIcon,object : Callback{
                        override fun onSuccess() {

                        }

                        override fun onError(e: Exception?) {
                            e?.printStackTrace()
                        }

                    })
            view.setOnClickListener { coinSelectedLiveData.value = coin }

        }

    }

    fun update(list: List<Coin>){
        this.list = list
        notifyDataSetChanged()
    }
}

