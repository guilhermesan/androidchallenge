package com.guilhermesan.thenextbigfin.ui.screens.currencydetail

import android.app.Activity
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import android.view.View
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.thenextbigfin.R
import com.guilhermesan.thenextbigfin.extencions.formatCurrency
import com.guilhermesan.thenextbigfin.extencions.formatVariation
import com.guilhermesan.thenextbigfin.ui.screens.BaseActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_currency_detail.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.view_market_values_wrapper.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor
import javax.inject.Inject

class CurrencyDetailActivity : BaseActivity() {

    companion object {
        private const val KEY_EXTRA_CURRENCY_SYMBOL = "key_extra_currency_symbol"

        fun start(activity: Activity, coin: Coin) {
            activity.startActivity<CurrencyDetailActivity>(KEY_EXTRA_CURRENCY_SYMBOL to coin.symbol)
            activity.overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out)
        }
    }

    @Inject
    lateinit var viewModel: CurrencyDetailViewModel

    override fun setup() {
        setupToolBar(toolbar,intent.getStringExtra(KEY_EXTRA_CURRENCY_SYMBOL),true)
    }

    override fun subscribe() {
        with(viewModel) {
            isLoading.observe(this@CurrencyDetailActivity, Observer {
                if (it != null && it){
                    loadingView.visibility = View.VISIBLE
                } else {
                    loadingView.visibility = View.GONE
                }
            })
            coinLiveData.observe(this@CurrencyDetailActivity, Observer { coinDetail ->
                if (coinDetail != null) {
                    tvAvailableSupply.text = coinDetail.availableSupply.formatCurrency()
                    tvMarketCap.text = coinDetail.marketCap.formatCurrency()
                    tvVolumeToday.text = coinDetail.todayVolume.formatCurrency()
                    tvCoinName.text = coinDetail.coin.name
                    tvVariation.text = coinDetail.coin.variation.formatVariation()
                    tvVariation.textColor = ContextCompat.getColor(
                            this@CurrencyDetailActivity,
                            if (coinDetail.coin.variation > 0)
                                R.color.green_positive
                            else
                                R.color.red_negative)

                    Picasso.get()
                            .load(coinDetail.coin.thumbUrl)
                            .into(ivCoinIcon)


                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
            overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
            return true
        }
        return false
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_detail)
        viewModel.onCreate(intent.getStringExtra(KEY_EXTRA_CURRENCY_SYMBOL))
        onCreated()
    }
}
