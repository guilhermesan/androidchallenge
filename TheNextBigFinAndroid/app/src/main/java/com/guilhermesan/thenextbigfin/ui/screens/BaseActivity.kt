package com.guilhermesan.thenextbigfin.ui.screens

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun onCreated(){
        setup()
        subscribe()
    }

    fun setupToolBar(toolbar: Toolbar, title:String, showBack:Boolean) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(showBack)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        supportActionBar?.title = ""
        toolbarTitle.text = title
    }

    abstract fun setup()
    abstract fun subscribe()
}