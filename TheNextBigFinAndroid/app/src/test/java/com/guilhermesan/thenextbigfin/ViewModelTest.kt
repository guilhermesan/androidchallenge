package com.guilhermesan.thenextbigfin

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.content.Context
import com.guilhermesan.datacontracts.dataproviders.CoinDataProvider
import com.guilhermesan.thenextbigfin.dagger.AppComponent
import com.guilhermesan.thenextbigfin.dagger.AppModule
import com.guilhermesan.thenextbigfin.ui.screens.currencydetail.CurrencyDetailViewModel
import com.guilhermesan.thenextbigfin.ui.screens.currencylist.CurrencyListViewModel
import dagger.Component
import dagger.Module
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Singleton





/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class ViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var app: TheNextBigFinApp

    @Mock
    lateinit var coinDataProvider: CoinDataProvider

    @Mock
    lateinit var appComponent: AppComponent

    lateinit var viewModel: CurrencyListViewModel



    @Before
    fun setupUp() {
        Mockito.`when`(app.appComponent).thenReturn(appComponent)
        viewModel = CurrencyListViewModel(app)
    }

    @Test
    fun testFetchList(){
        viewModel.search("")
        Mockito.verify(coinDataProvider).getCoins()
    }
}

@Module
class TestModule : AppModule() {


    fun provideContext(): Context {
        return Mockito.mock(TheNextBigFinApp::class.java)
    }


    fun providesCurrencyListViewModel(): CurrencyListViewModel {
        return Mockito.mock(CurrencyListViewModel::class.java)
    }


    fun providesCurrencyDetailViewModel(): CurrencyDetailViewModel {
        return Mockito.mock(CurrencyDetailViewModel::class.java)
    }


    fun providesCoinDataProvider(): CoinDataProvider {
        return Mockito.mock(CoinDataProvider::class.java)
    }

}

@Singleton
@Component(modules = [AppModule::class])
interface TestComponent : AppComponent {
    override fun inject(currencyListViewModel: CurrencyListViewModel);
}
