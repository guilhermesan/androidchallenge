package com.guilhermesan.apimodule.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.guilhermesan.datacontracts.vos.Coin
import java.lang.reflect.Type

class CoinDeserializer : JsonDeserializer<Coin> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Coin {
        val coinJson = json.asJsonObject
        val name = coinJson.get("long").asString
        val symbol = coinJson.get("short").asString
        val price = coinJson.get("price").asDouble
        val variation = coinJson.get("perc").asDouble
        val thumb = "http://coincap.io/images/coins/$name.png"
        val coin = Coin(name, symbol, price, variation, thumb)
        return coin
    }

}