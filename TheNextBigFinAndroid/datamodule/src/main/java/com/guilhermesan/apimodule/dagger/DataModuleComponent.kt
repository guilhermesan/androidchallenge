package com.guilhermesan.apimodule.dagger

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.guilhermesan.apimodule.api.Api
import com.guilhermesan.apimodule.deserializers.CoinDeserializer
import com.guilhermesan.apimodule.deserializers.CoinDetailDeserializer
import com.guilhermesan.apimodule.providers.CoinDataProviderImp
import com.guilhermesan.datacontracts.core.CacheManager
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Component(modules = [ApiModule::class])
interface DataModuleComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(application: Context): Builder

        fun build(): DataModuleComponent
    }

    fun inject(coinDataProvider: CoinDataProviderImp)

}

@Module
class ApiModule {

    @Provides
    fun provideOkHttp(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }

    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(Coin::class.java, CoinDeserializer())
                .registerTypeAdapter(CoinDetail::class.java, CoinDetailDeserializer())
                .create()
    }

    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://coincap.io/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
    }

    @Provides
    fun providesCoinApi(retrofit: Retrofit): Api {
        return  retrofit.create(Api::class.java)
    }

    @Provides
    fun providesCacheManager(context: Context): CacheManager{
        return CacheManager(context)
    }
}