package com.guilhermesan.apimodule.providers

import android.content.Context
import com.guilhermesan.apimodule.api.Api
import com.guilhermesan.apimodule.dagger.DaggerDataModuleComponent
import com.guilhermesan.datacontracts.core.CacheManager
import com.guilhermesan.datacontracts.dataproviders.CoinDataProvider
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import io.reactivex.Observable
import javax.inject.Inject

class CoinDataProviderImp(context: Context) : CoinDataProvider(context) {


    @Inject
    lateinit var api: Api

    @Inject
    lateinit var cacheManager: CacheManager

    init {
        DaggerDataModuleComponent
                .builder()
                .context(context)
                .build()
                .inject(this)
    }

    override fun getCoins(): Observable<List<Coin>> {
       return cacheManager
               .getCoins()
               .flatMap({ list ->
                   if (list.isEmpty()) getCoinsAPI() else Observable.just(list)
               })
    }



    fun getCoinsAPI():Observable<List<Coin>>{
        return api.getCoins().map {
            cacheManager.cacheCoins(it)
            it }
    }

    override fun getCoins(query: String): Observable<List<Coin>> {
        return getCoins().map {
            it.filter { it.name.toLowerCase().contains(query.toLowerCase()) }
        }
    }

    override fun getCoinDetail(symbol: String): Observable<CoinDetail> {
        return api.getCoinDetail(symbol)
    }

    override fun evictCache() {
        cacheManager.evictCoins()
    }


}