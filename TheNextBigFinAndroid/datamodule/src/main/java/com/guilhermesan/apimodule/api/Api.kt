package com.guilhermesan.apimodule.api

import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface Api {

    @GET("front")
    fun getCoins():Observable<List<Coin>>

    @GET("page/{coin}")
    fun getCoinDetail(@Path("coin") coin:String):Observable<CoinDetail>

}