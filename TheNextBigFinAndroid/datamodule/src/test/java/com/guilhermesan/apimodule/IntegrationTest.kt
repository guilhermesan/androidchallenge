package com.guilhermesan.apimodule

import com.guilhermesan.apimodule.api.Api
import com.guilhermesan.apimodule.dagger.ApiModule
import com.guilhermesan.datacontracts.vos.Coin
import com.guilhermesan.datacontracts.vos.CoinDetail
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner



/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */

@RunWith(MockitoJUnitRunner::class)
class IntegrationTest {

    lateinit var apiModule: ApiModule
    lateinit var api:Api

    var testSubscriberCoinList = TestObserver<List<Coin>>()
    var testSubscriberCoin = TestObserver<CoinDetail>()

    @Before
    fun setupAPI(){
        apiModule = ApiModule()
        api = apiModule.providesCoinApi(apiModule.provideRetrofit(
                        apiModule.provideGson(),apiModule.provideOkHttp()
                ))
    }

    @Test
    fun testGetCoins() {
        api.getCoins().subscribe(testSubscriberCoinList)
        testSubscriberCoinList.awaitTerminalEvent()
        testSubscriberCoinList.assertNoErrors()
        testSubscriberCoinList.assertComplete()
        testSubscriberCoinList.assertValueCount(1)
        assert(testSubscriberCoinList.values().isNotEmpty())
        Assert.assertEquals(testSubscriberCoinList.values()[0][0].name, "Bitcoin")

    }

    @Test
    fun testGetCoinDetail() {
        api.getCoinDetail("BTC").subscribe(testSubscriberCoin)
        testSubscriberCoin.awaitTerminalEvent()
        testSubscriberCoin.assertComplete()
        testSubscriberCoin.assertNoErrors()
        testSubscriberCoin.assertValueCount(1)
        Assert.assertEquals(testSubscriberCoin.values()[0].coin.name, "Bitcoin")
    }
}